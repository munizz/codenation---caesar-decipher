import requests
import json
import hashlib


class Challenge:
    def __init__(self, places=0, token='', encrypted='', decrypted='', resume=''):
        self.places = places
        self.token = token
        self.encrypted = encrypted
        self.decrypted = decrypted
        self.resume = resume

    def check_file(self):
        try:
            with open('answer.json', 'r') as f:
                f.close()
        except FileNotFoundError:
            self.get_file()

    def get_file(self):
        url = 'https://api.codenation.dev/v1/challenge/dev-ps/generate-data'
        response = requests.get(url, params={'token': self.token})

        with open('answer.json', 'w') as file:
            file.write(str(response.text))
            file.close()

    def read_file(self):
        with open('answer.json', 'r') as f:
            data = json.load(f)
            self.places = data['numero_casas']
            self.encrypted = data['cifrado']
            self.decrypt()

            return data

    def cipher(self, letter):
        if (self.places > 0 and '.' not in letter and ' ' not in letter and not letter.isnumeric()):
            return chr(ord(letter) + self.places)
        else:
            return letter

    def decrypt(self):
        encrypted_list = list(self.encrypted)
        decrypted = list(map(lambda c: self.cipher(c), encrypted_list))

        self.decrypted = ''.join(decrypted)
        self.resume_sha1()

    def resume_sha1(self):
        self.resume = hashlib.sha1(self.decrypted.encode('utf-8')).hexdigest()

    def save_data(self, data):
        with open('answer.json', 'w+') as f:
            data['decifrado'] = self.decrypted
            data['resumo_criptografico'] = str(self.resume)
            json.dump(data, fp=f)

    def upload_file(self):
        try:
            url = "https://api.codenation.dev/v1/challenge/dev-ps/submit-solution"
            answer = {"answer": open("./answer.json", "rb")}

            response = requests.request(
                "POST", url=url, files=answer, params={'token': self.token})

            print(response.text)
        except Exception as e:
            print(e)


if __name__ == '__main__':
    c = Challenge(token='5bde0d99bf53c395fd1a8b26c6985b6e92f670fa')
    c.check_file()
    c.save_data(c.read_file())
    c.upload_file()
